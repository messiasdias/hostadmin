<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', function () {
    return view('welcome');
});

Route::get('/teste/{id}',function($id){
	
	$id = strtolower($id);
	chdir("../../teste-dev");
	
	if ( is_dir($id) ){
		
		chdir("$id");
		if ( count(glob("index.*")) >= 1 ) {
			echo file_get_contents( glob("index.*")[0] );
		}else{
			abort(500, '401 - Ação Não autorizada, Falta o Arquivo index.* !');
		}

	}else{
		abort(404, '404 - Diretório ou Página Não Encontrada!');
	}

});